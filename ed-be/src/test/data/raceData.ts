import { DinozRace } from '../../models';

export const DinozRaceArray = [
	{
		raceId: 8654,
		name: 'pteroz',
		price: 22000
	},
	{
		raceId: 6541,
		name: 'rocky',
		price: 18000
	}
] as Array<DinozRace>;
