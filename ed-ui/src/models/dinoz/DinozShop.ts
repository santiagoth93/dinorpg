import { DinozRace } from '@/models';

export interface DinozShop {
	id: string;
	display: string;
	race: DinozRace;
}
