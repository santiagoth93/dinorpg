docker-start: docker-stop
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up -d --no-recreate

docker-watch:
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up --no-recreate

docker-stop:
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml stop

bash-front:
	docker exec -it drpg_front bash

bash-DB:
	docker exec -it drpg_database bash


build:
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml build
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up --no-start

install: build
	cp ./EternalTwin/etwin.toml.example ./EternalTwin/etwin.toml
	cp ./ed-be/config_dev.toml.example ./ed-be/config_dev.toml
	docker start drpg_database
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node drpg_front yarn install
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node drpg_back yarn install
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node drpg_eternal_twin yarn install
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml run -u node drpg_eternal_twin yarn etwin db upgrade
	docker-compose -f docker/docker-compose.yml -f docker/docker-compose.dev.yml up -d --no-recreate

install-eternal-twin: reset-eternal-twin-database
	docker start drpg_eternal_twin
	docker exec -i -unode drpg_eternal_twin yarn install

install-front:
	docker start drpg_front &&\
	docker exec -i -unode drpg_front yarn install

reset-eternal-twin-database:
	docker start drpg_database &&\
	cat docker/EternalTwin/drop.sql | docker exec -i drpg_database psql --username postgres eternal_twin &&\
	cat docker/EternalTwin/dump_12-01-2021_20_33_41.sql | docker exec -i drpg_database psql --username postgres eternal_twin

remove-drpg: docker-stop
	docker rm drpg_back
	docker rm drpg_database
	docker rm drpg_eternal_twin
	docker rm drpg_front

run-test: 
	docker exec -i -unode drpg_back yarn run test:ci

run-coverage:
	docker exec -i -unode drpg_back yarn run coverage

update-front:
	docker exec -i -unode drpg_front yarn install

test-lint-front:
	docker exec -i -unode drpg_front yarn lint --no-fix

fix-lint-front:
	docker exec -it drpg_front yarn lint
